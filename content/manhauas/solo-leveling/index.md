---
title: "Solo Leveling"
date: 2021-09-18T23:27:57-03:00
draft: false
front_page: "https://neoxscans.net/wp-content/uploads/2021/05/dsadsaad.jpg.webp"
resumo: "Dez anos atrás, depois do “Portal” que conecta o mundo real com um mundo de monstros se abriu, algumas pessoas comuns receberam o poder de caçar os monstros do portal. Eles são conhecidos como caçadores. Porém, nem todos os caçadores são fortes. Meu nome é Sung Jin-Woo, um caçador de rank E. Eu sou alguém que tem que arriscar a própria vida nas dungeons mais fracas, “O mais fraco do mundo”. Sem ter nenhuma habilidade à disposição, eu mal consigo dinheiro nas dungeons de baixo nível… Ao menos até eu encontrar uma dungeon escondida com a maior dificuldade dentro do Rank D! No fim, enquanto aceitava minha morte, eu ganhei um novo poder…"
link_ler: "https://neoxscans.net/manga/solo-leveling1"
---

# Primeiro Capitulo

![alt text](images/002.jpg)
![alt text](images/003.jpg)
![alt text](images/004.jpg)
![alt text](images/005.jpg)
![alt text](images/006.jpg)
![alt text](images/007.jpg)
![alt text](images/008.jpg)
![alt text](images/009.jpg)

Caso tenha gostado, leia mais no site da scan (o link esta na imagem de capa, ao lado do resumo)