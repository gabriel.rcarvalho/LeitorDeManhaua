---
title: "Seoul Station Druid"
date: 2021-09-18T23:27:57-03:00
draft: false
front_page: "https://neoxscans.net/wp-content/uploads/2021/06/SeoulCapa.png.webp"
resumo: "Park Sooho voltou para a Terra depois de desaparecer para outro mundo por 1.000 anos. Durante esse tempo, a Terra se transformou em um mundo de sobrevivência do mais forte graças a aparição de monstros. Druida, o rei dos animais, que conseguiu sobreviver por mil anos, agora vai conquistar o mundo."
link_ler: "https://neoxscans.net/manga/seoul-station-druid"
---

