# Bem vindo !

Esse é um site para todos que gostam de manhauas (ou mangas coreanos)

Aqui você vai encontrar alguns manhauas e seus respectivos resumos, além de um link para o *scan* que o traduz para português.

*Scan*s são organizações de fãns que por gostarem do conteúdo, se organizam para traduzir e disponibilizar gratuitamente.
Assim, gostaria de dar crédito as equipes que fazem ler esse conteudo possível.